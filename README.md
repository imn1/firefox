# n1's Firefox theme

- screenshots in "screenshot" folder
- highly inspired by [gruvbox](https://github.com/morhetz/gruvbox)

## Misc

- font: [Cozette](https://github.com/slavfox/Cozette/)
- startpage: [n1's startpage](https://gitlab.com/imn1/startpage)
- tabs addon: [Siteberry](https://addons.mozilla.org/en-US/firefox/addon/sidebery/)
