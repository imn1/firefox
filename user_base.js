user_pref("browser.bookmarks.max_backups", 0); // No bookmarks backup
user_pref("browser.cache.offline.enable", false); // Disable offline cache
user_pref("browser.newtabpage.activity-stream.asrouter.userprefs.cfr.addons", false);
user_pref("browser.newtabpage.activity-stream.asrouter.userprefs.cfr.features", false);
user_pref("browser.newtabpage.activity-stream.discoverystream.enabled", false);
user_pref("browser.newtabpage.activity-stream.feeds.section.highlights", false);
user_pref("browser.newtabpage.activity-stream.feeds.section.topstories", false);
user_pref("browser.newtabpage.activity-stream.feeds.snippets", false);
user_pref("browser.newtabpage.activity-stream.feeds.topsites", false);
user_pref("browser.newtabpage.activity-stream.showSearch", false);
user_pref("browser.newtabpage.enabled", false); // Disable newtab page
user_perf("browser.gesture.swipe.left", "Browser:ForwardOrForwardDuplicate") // gesture - swipe left
user_perf("browser.gesture.swipe.right", "Browser:BackOrBackDuplicate")  // gesture - swipe right

user_pref("network.dns.disablePrefetch", true); // Disable DNS prefetching
user_pref("network.prefetch-next", false); // Disable link prefetching

user_pref("beacon.enabled", false); // Disable beacon
user_pref("dom.battery.enabled", false); // Disable battery status
user_pref("dom.gamepad.enabled", false); // Disable USB devices
// user_pref("dom.push.enabled", false); // Disable push service
user_pref("dom.webnotifications.enabled", false); // Disable desktop notifications
user_pref("geo.provider.network.url", "");
// user_pref("media.peerconnection.enabled", false); // Disable WebRTC
user_pref("media.webspeech.synth.enabled", false); // Disable speech synthesis
user_pref("privacy.firstparty.isolate", true); // Restrict data to domain level

user_pref("app.normandy.api_url", "");
user_pref("app.normandy.enabled", false); // Disable shield telemetry
user_pref("app.shield.optoutstudies.enabled", false);
user_pref("datareporting.healthreport.uploadEnabled", false);
user_pref("datareporting.policy.dataSubmissionEnabled", false); // Disable all technical data
user_pref("datareporting.policy.firstRunURL", "");
user_pref("toolkit.telemetry.server", "");
user_pref("toolkit.telemetry.unified", false); // Disable Fx telemetry

//user_pref("extensions.pocket.enabled", false); // Disable pocket
user_pref("media.eme.enabled", false); // Disable EME for DRM content

user_pref("browser.aboutConfig.showWarning", false); // Disable modify warning
user_pref("browser.uidensity", 1); // Compact density
user_pref("findbar.highlightAll", true); // Highlight text search
// user_pref("general.smoothScroll", false); // Smooth scrolling off
user_pref("reader.parse-on-load.enabled", false); // Disable reader
user_pref("ui.key.menuAccessKeyFocuses", false); // Disable menu when ALT key is pressed
user_pref("mousewheel.system_scroll_override.enabled", true);
user_pref("mousewheel.system_scroll_override.horizontal.factor", 800);
user_pref("mousewheel.system_scroll_override.vertical.factor", 50);
user_pref("browser.translations.automaticallyPopup", false); // Disable translation popup
user_pref("media.ffmpeg.vaapi.enabled", true); // Disable EME for DRM content

user_pref("browser.fullscreen.autohide", false); // Do not hide toolbar in fullscreen
