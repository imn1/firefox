user_pref("font.name.serif.x-western", "Cozette");
user_pref("font.name.sans-serif.x-western", "Cozette");
user_pref("font.name.monospace.x-western", "Cozette");

user_pref("font.minimum-size.x-western", 13);
user_pref("font.size.fixed.x-western", 13);
user_pref("font.size.monospace.x-western", 13);
user_pref("font.size.variable.x-western", 13);

user_pref("mousewheel.default.delta_multiplier_y", 150);
user_pref("layout.css.devPixelsPerPx", "1.0");
